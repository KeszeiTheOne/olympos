<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Application\Competition\Action;

use Application\Competition\Action\AddCompetition\AddCompetitionAction;
use Application\Competition\Action\AddCompetition\AddCompetitionRequest;
use Application\Competition\Model\Competition;
use Application\Profile\Exception\MissingProfile;
use DateTime;
use Keszei\Action\Exception\InvalidRequest;
use Keszei\Action\Model\Response;
use Keszei\Action\Responder;
use Keszei\Action\Test\ActionTestCaseTrait;
use Keszei\Crud\Action\ModelResponse;
use Keszei\Crud\Test\Fixtures\Gateway\Persister\PersisterGatewaySpy;
use PHPUnit\Framework\TestCase;
use Tests\Application\Profile\Fixtures\ProfileProviderStub;

class AddCompetitionTest extends TestCase {

	use ActionTestCaseTrait;

	private $profile;

	private $profileProvider;

	/**
	 * @var PersisterGatewaySpy
	 */
	private $competitionGateway;

	protected function setUp() {
		parent::setUp();

		$this->profile = "profile";
		$this->profileProvider = $this->createProfileProvider($this->profile);
		$this->competitionGateway = new PersisterGatewaySpy();
	}

	private function createProfileProvider($profile) {
		$provider = new ProfileProviderStub();
		$provider->profile = $profile;

		return $provider;
	}

	/**
	 * @test
	 */
	public function unkownRequest() {
		$this->assertRunThrowsUnexpectedTypeWhenRequestIsUnknown();
	}

	/**
	 * @test
	 * @dataProvider invalidRequestProvider
	 */
	public function invalidRequest($param, $value) {
		$request = $this->request();
		$request->$param = $value;

		$this->assertRunThrows($request, InvalidRequest::class);
	}

	public function invalidRequestProvider() {
		return [
			["name", ""],
			["name", null],
			["date", ""],
			["date", null],
			["date", "string"],
			["place", ""],
			["place", null],
			["visibility", ""],
			["visibility", null],
			["visibility", "string"],
		];
	}

	/**
	 * @test
	 */
	public function missingProfile() {
		$this->profileProvider = $this->createProfileProvider(null);

		$this->assertRunThrows($this->request(), MissingProfile::class);
	}

	/**
	 * @test
	 */
	public function respondedResponse() {
		$response = $this->runAction($this->request());

		$this->assertInstanceOf(ModelResponse::class, $response);
	}

	/**
	 * @test
	 */
	public function respondedCompetition() {
		$response = $this->runAction($this->request());

		$this->assertInstanceOf(Competition::class, $competition = $response->getModel());
		$this->assertSame("name", $competition->getName());
		$this->assertSame("place", $competition->getPlace());
		$this->assertEquals(new DateTime("2019-01-01"), $competition->getDate());
		$this->assertSame(false, $competition->getVisibility());
		$this->assertSame($this->profile, $competition->getProfile());
	}

	/**
	 * @test
	 */
	public function persistedCompetition() {
		$response = $this->runAction($this->request());

		$this->assertCount(1, $this->competitionGateway->objects);
		$this->assertSame($response->getModel(), $this->competitionGateway->objects[0]);
	}

	protected function runAction($request): Response {
		(new AddCompetitionAction($responder = new Responder()))
			->setProfileProvider($this->profileProvider)
			->setCompetitionGateway($this->competitionGateway)
			->run($request);

		return $responder->getResponse();
	}

	private function request() {
		$request = new AddCompetitionRequest();
		$request->name = "name";
		$request->place = "place";
		$request->date = new DateTime("2019-01-01");
		$request->visibility = false;

		return $request;
	}

}
