<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Application\Competition\Action;

use Application\Competition\Action\ListCompetitions\ListCompetitionsAction;
use Application\Competition\Action\ListCompetitions\ListCompetitionsRequest;
use Application\Competition\Gateway\CompetitionCriteria;
use Application\Competition\Model\Competition;
use Application\Profile\Exception\MissingProfile;
use Keszei\Action\Exception\UnexpectedType;
use Keszei\Action\Model\Response;
use Keszei\Action\Responder;
use Keszei\Action\Test\ActionTestCaseTrait;
use Keszei\Crud\Test\Fixtures\Gateway\Filtering\FilteringGatewaySpy;
use Keszei\Crud\Test\Fixtures\Model\ModelDummy;
use PHPUnit\Framework\TestCase;
use Tests\Application\Profile\Fixtures\ProfileProviderStub;

class ListCompetitionsTest extends TestCase {

	use ActionTestCaseTrait;

	private $competitions = [];

	/**
	 * @var FilteringGatewaySpy
	 */
	private $competitionGateway;

	/**
	 * @var ProfileProviderStub
	 */
	private $profileProvider;

	protected function setUp() {
		parent::setUp();
		$this->profileProvider = new ProfileProviderStub();
		$this->profileProvider->profile = "profile";
	}

	/**
	 * @test
	 */
	public function unknownRequest() {
		$this->assertRunThrowsUnexpectedTypeWhenRequestIsUnknown();
	}

	/**
	 * @test
	 */
	public function filteredCompetitions() {
		$this->runAction($this->request());

		$this->assertSame(1, $this->competitionGateway->getFilteringTimes());
		$this->assertInstanceOf(CompetitionCriteria::class, $criteria = $this->competitionGateway->getLastFilteredCriteria());
		$this->assertSame("profile", $criteria->getProfile());
	}
	
	/**
	 * @test
	 */
	public function missingProfile() {
		$this->profileProvider->profile = null;
		
		$this->assertRunThrows($this->request(), MissingProfile::class);
	}

	/**
	 * @test
	 */
	public function unknownCompetition() {
		$this->competitions[] = new ModelDummy();

		$this->assertRunThrows($this->request(), UnexpectedType::class);
	}

	/**
	 * @test
	 */
	public function respondedCompetitions() {
		$this->competitions[] = new Competition();

		$response = $this->runAction($this->request());

		$this->assertCount(1, $response);
		$this->assertSame($this->competitions[0], $response->getModels()[0]);
	}

	protected function runAction($request): Response {
		$this->competitionGateway = new FilteringGatewaySpy($this->competitions);
		(new ListCompetitionsAction($responder = new Responder()))
			->setCompetitionGateway($this->competitionGateway)
			->setProfileProvider($this->profileProvider)
			->run($request);

		return $responder->getResponse();
	}

	private function request() {
		return new ListCompetitionsRequest();
	}

}
