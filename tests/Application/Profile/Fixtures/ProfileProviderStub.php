<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Application\Profile\Fixtures;

use Application\Profile\Gateway\ProfileProvider;

class ProfileProviderStub implements ProfileProvider {

	public $profile;

	public function getProfile() {
		return $this->profile;
	}

}
