<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Keszei\Scenario\Fixtures;

use Exception;
use Keszei\Scenario\Act;

class ActSpy implements Act {

	public $stimulises = [];

	public $story;

	public function play($stimulus) {
		$this->stimulises[] = $stimulus;

		if ($this->story instanceof Exception) {
			throw $this->story;
		}

		return $this->story;
	}

}
