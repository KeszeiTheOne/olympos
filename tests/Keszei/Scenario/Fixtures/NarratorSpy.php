<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Keszei\Scenario\Fixtures;

use Keszei\Scenario\Narrator;

class NarratorSpy implements Narrator {

	public $narration;

	public $stimuluses = [];

	public $stories = [];

	public function tellStory($stimulus, $story) {
		$this->stimuluses[] = $stimulus;
		$this->stories[] = $story;

		return $this->narration;
	}

}
