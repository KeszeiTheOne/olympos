<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Keszei\Scenario\Fixtures;

use Keszei\Scenario\Exception\UnexpectedNarrative;
use Keszei\Scenario\Publisher;

class PublisherSpy implements Publisher {

	public $response;

	public $narratives = [];

	public $exception = false;

	public function publishNarrative($narrative) {
		$this->narratives[] = $narrative;
		if ($this->exception) {
			throw new UnexpectedNarrative;
		}

		return $this->response;
	}

}
