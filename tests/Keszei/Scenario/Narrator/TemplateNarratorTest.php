<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Keszei\Scenario\Narrator;

use Keszei\Scenario\Narrator\Template\Template;
use Keszei\Scenario\Narrator\Template\TemplateNarrator;
use PHPUnit\Framework\TestCase;
use Tests\Keszei\Scenario\Fixtures\ParameterResolverSpy;

class TemplateNarratorTest extends TestCase {

	private $parameters;

	protected function setUp() {
		parent::setUp();
		$this->parameters = ["template" => "param"];
	}

	/**
	 * @test
	 */
	public function narratedTemplate() {
		$template = $this->tellStory();

		$this->assertInstanceOf(Template::class, $template);
		$this->assertSame("name", $template->getName());
		$this->assertSame(["template" => "param"], $template->getParameters());
		$this->assertSame(404, $template->getStatusCode());
	}

	/**
	 * @test
	 */
	public function parametersIsParameterResolver() {
		$this->parameters = new ParameterResolverSpy();
		$this->parameters->parameters = ["resolved" => "params"];

		$template = $this->tellStory();

		$this->assertCount(1, $this->parameters->stimuluses);
		$this->assertSame("stimulus", $this->parameters->stimuluses[0]);
		$this->assertSame("story", $this->parameters->stories[0]);
		$this->assertSame(["resolved" => "params"], $template->getParameters());
	}

	private function tellStory() {
		return (new TemplateNarrator("name", $this->parameters, 404))->tellStory("stimulus", "story");
	}

}
