<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Keszei\Scenario;

use Keszei\Scenario\Scenario;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use Tests\Keszei\Scenario\Fixtures\ActSpy;
use Tests\Keszei\Scenario\Fixtures\NarratorSpy;

class ScenarioTest extends TestCase {

	/**
	 * @var ActSpy
	 */
	private $act;

	private $stimulus;

	/**
	 * @var NarratorSpy
	 */
	private $narrator;

	/**
	 * @var NarratorSpy[]
	 */
	private $exceptionalNarrators = [];

	protected function setUp() {
		parent::setUp();
		$this->act = new ActSpy();
		$this->stimulus = "stimulus";
		$this->narrator = new NarratorSpy();
	}

	private function addExceptionNarrator($exceptionClass, $narrator) {
		$this->exceptionalNarrators[$exceptionClass] = $narrator;
	}

	/**
	 * @test
	 */
	public function stimulusTelledToAct() {
		$this->play();

		$this->assertCount(1, $this->act->stimulises);
		$this->assertSame($this->stimulus, $this->act->stimulises[0]);
	}

	/**
	 * @test
	 */
	public function storyTelledToNarrator() {
		$this->act->story = "story";

		$this->play();

		$this->assertCount(1, $this->narrator->stimuluses);
		$this->assertSame($this->stimulus, $this->narrator->stimuluses[0]);
		$this->assertSame("story", $this->narrator->stories[0]);
	}

	/**
	 * @test
	 */
	public function narrationPlayed() {
		$this->narrator->narration = "narration";

		$narration = $this->play();

		$this->assertSame("narration", $narration);
	}

	/**
	 * @test
	 */
	public function exceptionAndNotExistNarration() {
		$this->act->story = new RuntimeException();

		$this->expectException(RuntimeException::class);
		$this->play();
	}

	/**
	 * @test
	 */
	public function existExceptionNarrator() {
		$narrator = new NarratorSpy();
		$this->addExceptionNarrator(RuntimeException::class, $narrator);
		$this->act->story = $exception = new RuntimeException();

		$this->play();

		$this->assertCount(1, $narrator->stimuluses);
		$this->assertCount(0, $this->narrator->stimuluses);
		$this->assertSame($this->stimulus, $narrator->stimuluses[0]);
		$this->assertSame($exception, $narrator->stories[0]);
	}

	/**
	 * @test
	 */
	public function exceptionNarratorTelledStory() {
		$narrator = new NarratorSpy();
		$narrator->narration = "exceptionNarration";
		$this->addExceptionNarrator(RuntimeException::class, $narrator);
		$this->act->story = new RuntimeException();

		$narration = $this->play();

		$this->assertSame("exceptionNarration", $narration);
	}

	private function play() {
		return (new Scenario($this->act, $this->narrator))
				->setExceptionalNarrators($this->exceptionalNarrators)
				->play($this->stimulus);
	}

}
