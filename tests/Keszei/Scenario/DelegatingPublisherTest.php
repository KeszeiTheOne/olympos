<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Keszei\Scenario;

use Keszei\Scenario\DelegatingPublisher;
use Keszei\Scenario\Exception\UnexpectedNarrative;
use PHPUnit\Framework\TestCase;
use Tests\Keszei\Scenario\Fixtures\PublisherSpy;

class DelegatingPublisherTest extends TestCase {

	/**
	 * @var PublisherSpy[] 
	 */
	private $publishers = [];

	/**
	 * @test
	 */
	public function notExistPublishers() {
		$this->expectException(UnexpectedNarrative::class);
		$this->publishNarrative("narrative");
	}

	/**
	 * @test
	 */
	public function firstPublishingPublisher() {
		$publisher = new PublisherSpy();
		$publisher->response = "response";
		$this->publishers[] = $publisher;

		$response = $this->publishNarrative("narrative");

		$this->assertCount(1, $publisher->narratives);
		$this->assertSame("narrative", $publisher->narratives[0]);
		$this->assertSame("response", $response);
	}

	/**
	 * @test
	 */
	public function secondPublishingPublisher() {
		$notPublish = new PublisherSpy();
		$notPublish->exception = true;
		$publisher = new PublisherSpy();
		$publisher->response = "response";
		$this->publishers[] = $notPublish;
		$this->publishers[] = $publisher;

		$response = $this->publishNarrative("narrative");

		$this->assertCount(1, $notPublish->narratives);
		$this->assertCount(1, $publisher->narratives);
		$this->assertSame("narrative", $publisher->narratives[0]);
		$this->assertSame("response", $response);
	}

	private function publishNarrative($narrative) {
		return (new DelegatingPublisher())
				->setPublishers($this->publishers)
				->publishNarrative($narrative);
	}

}
