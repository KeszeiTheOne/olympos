<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\AppBundle\Controller\Competition\AddCompetition;

use AppBundle\Controller\Competition\AddCompetition\AddCompetitionType;
use Application\Competition\Action\AddCompetition\AddCompetitionRequest;
use Symfony\Component\Form\Test\TypeTestCase;

class AddCompetitionTypeTest extends TypeTestCase {

	private function createForm() {
		return $this->factory->create(AddCompetitionType::class);
	}

	/**
	 * @test
	 */
	public function formDataIsNull() {
		$this->assertNull($this->createForm()->getData());
	}

	/**
	 * @test
	 */
	public function emptySubmit() {
		$request = $this->createForm()->submit([])->getData();

		$this->assertInstanceOf(AddCompetitionRequest::class, $request);
	}

	/**
	 * @test
	 */
	public function submitName() {
		$request = $this->createForm()->submit([
				"name" => "Name"
			])->getData();

		$this->assertSame("Name", $request->name);
	}

	/**
	 * @test
	 */
	public function submitPlace() {
		$request = $this->createForm()->submit([
				"place" => "place"
			])->getData();

		$this->assertSame("place", $request->place);
	}

	/**
	 * @test
	 */
	public function submitDate() {
		$request = $this->createForm()->submit([
				"date" => "2019.01.01"
			])->getData();

		$this->assertEquals(new \DateTime("2019-01-01"), $request->date);
	}

	/**
	 * @test
	 */
	public function submitVisibility() {
		$request = $this->createForm()->submit([
				"visibility" => 1
			])->getData();

		$this->assertSame(true, $request->visibility);
	}

}
