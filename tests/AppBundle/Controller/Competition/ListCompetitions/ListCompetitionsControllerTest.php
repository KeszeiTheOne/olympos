<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\AppBundle\Controller\Competition\ListCompetitions;

use AppBundle\Controller\Competition\ListCompetition\ListCompetitionsController;
use Application\Profile\Exception\MissingProfile;
use Keszei\Scenario\Narrator\ClientError\ClientError;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Tests\Keszei\Scenario\Fixtures\ActSpy;

class ListCompetitionsControllerTest extends TestCase {

	private $act;

	private $request;

	protected function setUp() {
		parent::setUp();
		$this->act = new ActSpy();
		$this->request = new Request();
	}

	/**
	 * @test
	 */
	public function missingProfile() {
		$this->act->story = new MissingProfile();

		$tale = $this->play();

		$this->assertInstanceOf(ClientError::class, $tale);
		$this->assertSame(403, $tale->getStatusCode());
	}

	/**
	 * @test
	 */
	public function templateRendered() {
		$tale = $this->play();

		$this->assertSame("app/competition/list.html.twig", $tale->getName());
		$this->assertSame(["fake" => "param"], $tale->getParameters());
	}

	private function play() {
		return (new ListCompetitionsControllerFake())
				->buildScenario($this->act)
				->play($this->request);
	}

}

class ListCompetitionsControllerFake extends ListCompetitionsController {

	protected function resolveParameters() {
		return ["fake" => "param"];
	}

}
