<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Application;

use Application\Competition\Action\CompetitionActionFactoryPackage;

class AppActionFactories {

	public static function getActionFactories() {
		return [
			"competition" => new CompetitionActionFactoryPackage()
		];
	}

}
