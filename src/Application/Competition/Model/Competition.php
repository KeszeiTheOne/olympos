<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Application\Competition\Model;

class Competition {

	private $id;

	private $name;

	private $place;

	private $date;

	private $profile;

	private $visibility;

	public function getId() {
		return $this->id;
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	public function getPlace() {
		return $this->place;
	}

	public function setPlace($place) {
		$this->place = $place;
		return $this;
	}

	public function getDate() {
		return $this->date;
	}

	public function setDate($date) {
		$this->date = $date;
		return $this;
	}

	public function getProfile() {
		return $this->profile;
	}

	public function setProfile($profile) {
		$this->profile = $profile;
		return $this;
	}

	public function getVisibility() {
		return $this->visibility;
	}

	public function setVisibility($visibility) {
		$this->visibility = $visibility;
		return $this;
	}

}
