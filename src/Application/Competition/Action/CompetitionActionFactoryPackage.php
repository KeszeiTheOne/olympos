<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Application\Competition\Action;

use Application\Competition\Action\AddCompetition\AddCompetitionActionFactory;
use Application\Competition\Action\ListCompetitions\ListCompetitionsActionFactory;
use Keszei\Action\Model\ActionFactoryPackage;

class CompetitionActionFactoryPackage implements ActionFactoryPackage {

	public function getActionFactories() {
		return [
			"add"	 => new AddCompetitionActionFactory(),
			"list"	 => new ListCompetitionsActionFactory()
		];
	}

}
