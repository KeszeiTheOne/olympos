<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Application\Competition\Action\ListCompetitions;

use Application\Competition\Gateway\CompetitionCriteria;
use Application\Competition\Model\Competition;
use Application\Profile\Exception\MissingProfile;
use Application\Profile\Gateway\ProfileProvider;
use Keszei\Action\AbstractAction;
use Keszei\Action\Exception\UnexpectedType;
use Keszei\Action\Model\Request;
use Keszei\Crud\Action\ListResponse;
use Keszei\Crud\Gateway\FilteringGateway;
use Keszei\Crud\Gateway\TypeCheckedFilteringGateway;

class ListCompetitionsAction extends AbstractAction {

	/**
	 * @var ProfileProvider
	 */
	private $profileProvider;

	/**
	 * @var FilteringGateway
	 */
	private $competitionGateway;

	public function run(Request $request) {
		if (!$request instanceof ListCompetitionsRequest) {
			throw new UnexpectedType;
		}

		$this->respond($this->filterCompetitions());
	}

	private function filterCompetitions() {
		$criteria = new CompetitionCriteria();
		$criteria->setProfile($this->getProfile());

		return $this->competitionGateway->filter($criteria);
	}
	
	private function getProfile() {
		$profile = $this->profileProvider->getProfile();
		
		if (null === $profile) {
			throw new MissingProfile;
		}
		
		return $profile;
	}

	private function respond($competitions) {
		$this->getResponder()->setResponse(new ListResponse($competitions));
	}

	public function setProfileProvider(ProfileProvider $profileProvider) {
		$this->profileProvider = $profileProvider;
		return $this;
	}

	public function setCompetitionGateway(FilteringGateway $competitionGateway) {
		$this->competitionGateway = new TypeCheckedFilteringGateway($competitionGateway, Competition::class);
		return $this;
	}

}
