<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Application\Competition\Action\AddCompetition;

use Application\Action\AbstractActionFactory;
use Application\Action\ActionConfig;
use Application\Competition\Model\Competition;
use Keszei\Action\Responder;

class AddCompetitionActionFactory extends AbstractActionFactory {

	protected function createActionWithActionConfig(Responder $responder, ActionConfig $config) {
		return (new AddCompetitionAction($responder))
				->setCompetitionGateway($config->getGateway(Competition::class))
				->setProfileProvider($config->getProfileProvider());
	}

}
