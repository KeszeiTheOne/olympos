<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Application\Competition\Action\AddCompetition;

use Application\Competition\Model\Competition;
use Application\Profile\Exception\MissingProfile;
use Application\Profile\Gateway\ProfileProvider;
use Keszei\Action\AbstractAction;
use Keszei\Action\Exception\InvalidRequest;
use Keszei\Action\Exception\UnexpectedType;
use Keszei\Action\Model\Request;
use Keszei\Crud\Action\ModelResponse;
use Keszei\Crud\Gateway\PersisterGateway;

class AddCompetitionAction extends AbstractAction {

	/**
	 * @var ProfileProvider
	 */
	private $profileProvider;

	/**
	 * @var PersisterGateway
	 */
	private $competitionGateway;

	public function run(Request $request) {
		$request = $this->ensureRequest($request);

		$competition = new Competition();
		$competition->setName($request->name);
		$competition->setPlace($request->place);
		$competition->setDate($request->date);
		$competition->setProfile($this->getProfile());
		$competition->setVisibility($request->visibility);

		$this->persistAndRespond($competition);
	}

	private function ensureRequest($request) {
		if (!$request instanceof AddCompetitionRequest) {
			throw new UnexpectedType;
		}
		if (!$request->isValid()) {
			throw new InvalidRequest($request);
		}

		return $request;
	}

	private function getProfile() {
		$profile = $this->profileProvider->getProfile();
		if (null === $profile) {
			throw new MissingProfile;
		}

		return $profile;
	}

	private function persistAndRespond($competition) {
		$this->competitionGateway->persist($competition);

		$this->getResponder()->setResponse(new ModelResponse($competition));
	}

	public function setProfileProvider(ProfileProvider $profileProvider) {
		$this->profileProvider = $profileProvider;
		return $this;
	}

	public function setCompetitionGateway(PersisterGateway $competitionGateway) {
		$this->competitionGateway = $competitionGateway;
		return $this;
	}

}
