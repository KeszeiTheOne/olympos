<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Application\Competition\Action\AddCompetition;

use DateTime;
use Keszei\Action\Model\Request;

class AddCompetitionRequest implements Request {

	public $name;

	public $date;

	public $place;

	public $visibility;

	public function isValid() {
		return !empty($this->name) &&
			$this->date instanceof DateTime &&
			!empty($this->place) &&
			is_bool($this->visibility);
	}

}
