<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Application\Competition\Gateway;

class CompetitionCriteria {

	private $profile;

	public function getProfile() {
		return $this->profile;
	}

	public function setProfile($profile) {
		$this->profile = $profile;
		return $this;
	}

}
