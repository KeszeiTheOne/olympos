<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Application\Action;

use Application\Profile\Gateway\ProfileProvider;
use Keszei\Action\Model\Config;
use Keszei\Crud\Gateway\CrudGateway;

interface ActionConfig extends Config {

	/**
	 * @return CrudGateway 
	 */
	public function getGateway($className);

	/**
	 * @return ProfileProvider 
	 */
	public function getProfileProvider();
}
