<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Application\Action;

use Keszei\Action\Model\ActionFactory;
use Keszei\Action\Model\Config;
use Keszei\Action\Responder;

abstract class AbstractActionFactory implements ActionFactory {

	public function createAction(Responder $responder, Config $config) {
		return $this->createActionWithActionConfig($responder, $config);
	}

	abstract protected function createActionWithActionConfig(Responder $responder, ActionConfig $config);
}
