<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Action;

use AppBundle\Doctrine\CrudGateway;
use Application\Action\ActionConfig;
use Application\Profile\Gateway\ProfileProvider;
use Doctrine\Bundle\DoctrineBundle\Registry;
use RuntimeException;
use Symfony\Component\DependencyInjection\Container;

class AppActionConfig implements ActionConfig {

	/**
	 * @var Container
	 */
	private $container;

	public function __construct(Container $container) {
		$this->container = $container;
	}

	public function getGateway($className): CrudGateway {
		$gateway = $this->getDoctrine()->getManagerForClass($className)->getRepository($className);

		if (!$gateway instanceof CrudGateway) {
			throw new RuntimeException("The repository of $className is not a CrudGateway instance.");
		}
		$gateway->setCriteriaProcessor($this->get("app.criteria_processors"));

		return $gateway;
	}

	public function getProfileProvider(): ProfileProvider {
		return new ProfileProviderDummy();
	}

	/**
	 * @return Registry 
	 */
	private function getDoctrine() {
		return $this->get("doctrine");
	}

	private function get($id) {
		return $this->container->get($id);
	}

}

class ProfileProviderDummy implements ProfileProvider {

	public function getProfile() {
		return "profile";
	}

}
