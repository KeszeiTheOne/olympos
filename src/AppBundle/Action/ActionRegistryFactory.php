<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Action;

use Application\AppActionFactories;
use Keszei\Action\ActionFactoryCollector;
use Keszei\Action\ActionRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ActionRegistryFactory {

	public static function create(ContainerInterface $container) {
		return new ActionRegistry($container->get("app.action.config"), self::getActionFactories());
	}

	private static function getActionFactories() {
		return (new ActionFactoryCollector(AppActionFactories::getActionFactories()))->getIterator();
	}

}
