<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Translator;

use Symfony\Component\Translation\MessageCatalogueInterface;
use Symfony\Component\Translation\Translator as BaseTranslator;
use Symfony\Component\Translation\TranslatorBagInterface;
use Symfony\Component\Translation\TranslatorInterface;

class Translator implements TranslatorInterface, TranslatorBagInterface {

	private $defaultDomain = "olympos";

	/**
	 * @var BaseTranslator
	 */
	private $translator;

	public function __construct(BaseTranslator $translator) {
		$this->translator = $translator;
	}

	public function trans($id, array $parameters = array(), $domain = null, $locale = null) {
		if (null === $domain || "messages" === $domain) {
			$domain = $this->defaultDomain;
		}

		return $this->translator->trans($id, $parameters, $domain, $locale);
	}

	public function transChoice($id, $number, array $parameters = array(), $domain = null, $locale = null) {
		if (null === $domain || "messages" === $domain) {
			$domain = $this->defaultDomain;
		}

		return $this->translator->transChoice($id, $number, $parameters, $domain, $locale);
	}

	public function getDefaultDomain() {
		return $this->defaultDomain;
	}

	public function setDefaultDomain($defaultDomain) {
		$this->defaultDomain = $defaultDomain;
		return $this;
	}

	public function getCatalogue($locale = null): MessageCatalogueInterface {
		return $this->translator->getCatalogue($locale);
	}

	public function getLocale(): string {
		return $this->translator->getLocale();
	}

	public function setLocale($locale) {
		$this->translator->setLocale($locale);
	}

}
