<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use Keszei\Scenario\Action\ActionAct;
use Keszei\Scenario\DelegatingPublisher;
use Keszei\Scenario\RequestConverter;
use Keszei\Scenario\Symfony\ClientError\ClientErrorPublisher;
use Keszei\Scenario\Symfony\Form\FormRequestConverter;
use Keszei\Scenario\Symfony\RedirectToRoute\RedirectToRoutePublisher;
use Keszei\Scenario\Symfony\Template\TemplatePublisher;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as SymfonyController;

abstract class Controller extends SymfonyController {

	protected function publishNarrative($narrative) {
		return (new DelegatingPublisher())
				->addPublisher(new TemplatePublisher($this->get("twig")))
				->addPublisher(new RedirectToRoutePublisher($this->get("router")))
				->addPublisher(new ClientErrorPublisher())
				->publishNarrative($narrative);
	}

	protected function createActionAct($actionName, RequestConverter $requestConverter) {
		return (new ActionAct())
				->setActionRegistry($this->get("app.action.registry"))
				->setActionName($actionName)
				->setRequestConverter($requestConverter);
	}

	protected function createFormRequestConverter($type) {
		return (new FormRequestConverter($this->get("form.factory")))
				->setType($type);
	}

	protected function generateBreadcrumbs(array $items = []) {
		$breadcrumbs = $this->get("white_october_breadcrumbs");
		$breadcrumbs->addRouteItem("Home", "homepage");
		foreach ($items as $name => $route) {
			$breadcrumbs->addRouteItem($name, $route);
		}

		return $breadcrumbs;
	}

}
