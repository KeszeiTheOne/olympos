<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Controller\Competition\AddCompetition;

use Application\Competition\Action\AddCompetition\AddCompetitionRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddCompetitionType extends AbstractType {

	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add("name", TextType::class);
		$builder->add("place", TextType::class);

		$builder->add("date", DateType::class, [
			'widget' => 'single_text',
			'format' => 'yyyy.MM.dd',
		]);

		$builder->add("visibility", CheckboxType::class, [
			"required" => false
		]);
	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults([
			"data_class" => AddCompetitionRequest::class
		]);
	}

}
