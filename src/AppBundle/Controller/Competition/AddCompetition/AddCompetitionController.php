<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Controller\Competition\AddCompetition;

use AppBundle\Controller\Controller;
use Keszei\Scenario\Action\CallbackParameterResolver;
use Keszei\Scenario\Narrator\RedirectToRoute\RedirectToRouteNarrator;
use Keszei\Scenario\Narrator\Template\TemplateNarrator;
use Keszei\Scenario\Scenario;
use Keszei\Scenario\Symfony\Form\Exception\ErroneousForm;
use Symfony\Component\HttpFoundation\Request;

class AddCompetitionController extends Controller {

	public function __invoke(Request $httpRequest) {
//		$this->generateBreadcrumbs(["competition.add.title" => "add_competition"]);
		$act = $this->createActionAct("competition.add", $this->createFormRequestConverter(AddCompetitionType::class));

		return $this->publishNarrative($this->buildScenario($act)->play($httpRequest));
	}

	private function buildScenario($act) {
		return (new Scenario($act, new RedirectToRouteNarrator("list_competition")))
				->addExceptionalNarrator(ErroneousForm::class, new TemplateNarrator("app/competition/add.html.twig", $this->createTemplateParameter()));
	}

	private function createTemplateParameter() {
		return new CallbackParameterResolver(function($stimulus, $story) {
			return [
				"form" => $story->getForm()->createView()
			];
		});
	}

}
