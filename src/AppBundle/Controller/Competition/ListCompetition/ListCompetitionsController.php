<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Controller\Competition\ListCompetition;

use AppBundle\Controller\Controller;
use Application\Competition\Action\ListCompetitions\ListCompetitionsRequest;
use Application\Profile\Exception\MissingProfile;
use Keszei\Scenario\Action\CallbackParameterResolver;
use Keszei\Scenario\Narrator\ClientError\AccessDeniedNarrator;
use Keszei\Scenario\Narrator\Template\TemplateNarrator;
use Keszei\Scenario\Scenario;
use Keszei\Scenario\Symfony\Form\CallbackRequestConverter;
use Symfony\Component\HttpFoundation\Request;

class ListCompetitionsController extends Controller {

	public function __invoke(Request $httpRequest) {
		$act = $this->createActionAct("competition.list", $this->createRequestConverter());

		return $this->publishNarrative($this->buildScenario($act)->play($httpRequest));
	}

	private function createRequestConverter() {
		return new CallbackRequestConverter(function() {
			return new ListCompetitionsRequest();
		});
	}

	public function buildScenario($act) {
		return (new Scenario($act, new TemplateNarrator("app/competition/list.html.twig", $this->resolveParameters())))
				->addExceptionalNarrator(MissingProfile::class, new AccessDeniedNarrator());
	}

	protected function resolveParameters() {
		return new CallbackParameterResolver(function($stimulus, $story) {
			return [
				"competitions" => $story
			];
		});
	}

}
