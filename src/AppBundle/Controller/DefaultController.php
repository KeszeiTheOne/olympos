<?php

namespace AppBundle\Controller;

use Keszei\Scenario\Act;
use Keszei\Scenario\Narrator\Template\TemplateNarrator;
use Keszei\Scenario\Scenario;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {

	public function indexAction(Request $request) {
		return $this->publishNarrative($this->buildScenario(new ActDummy())->play($request));
	}

	private function buildScenario($act) {
		return new Scenario($act, new TemplateNarrator("default/index.html.twig", [
			'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
		]));
	}

}

class ActDummy implements Act {

	public function play($stimulus) {
		
	}

}
