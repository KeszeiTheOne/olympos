<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Doctrine;

use Doctrine\ORM\EntityRepository;
use Keszei\Crud\Gateway\CrudGateway as BaseCrudGateway;

class CrudGateway extends EntityRepository implements BaseCrudGateway {

	/**
	 * @var CriteriaProcessor
	 */
	private $criteriaProcessor;

	public function find($id, $lockMode = null, $lockVersion = null) {
		if (is_object($id)) {
			$qb = $this->createQueryBuilder("crud");
			$this->criteriaProcessor->process($qb, $id);

			return $qb->getQuery()->getOneOrNullResult();
		}

		return parent::find($id, $lockMode, $lockVersion);
	}

	public function filter($criteria) {
		$qb = $this->createQueryBuilder("crud");
		$this->criteriaProcessor->process($qb, $criteria);

		return $qb->getQuery()->getResult();
	}

	public function persist($object) {
		$this->getEntityManager()->persist($object);
		$this->getEntityManager()->flush();
	}

	public function remove($object) {
		$this->getEntityManager()->remove($object);
		$this->getEntityManager()->flush();
	}

	public function setCriteriaProcessor(CriteriaProcessor $criteriaProcessor) {
		$this->criteriaProcessor = $criteriaProcessor;
	}

}
