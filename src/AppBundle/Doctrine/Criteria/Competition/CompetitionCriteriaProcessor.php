<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Doctrine\Criteria\Competition;

use AppBundle\Doctrine\CannotProcessCriteria;
use AppBundle\Doctrine\CriteriaProcessor;
use Application\Competition\Gateway\CompetitionCriteria;
use Doctrine\ORM\QueryBuilder;

class CompetitionCriteriaProcessor implements CriteriaProcessor {

	public function process(QueryBuilder $queryBuilder, $criteria) {
		if (!$criteria instanceof CompetitionCriteria) {
			throw new CannotProcessCriteria;
		}
	}

}
