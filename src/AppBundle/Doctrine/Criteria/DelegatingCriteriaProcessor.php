<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Doctrine\Criteria;

use AppBundle\Doctrine\CannotProcessCriteria;
use AppBundle\Doctrine\CriteriaProcessor;
use Doctrine\ORM\QueryBuilder;

class DelegatingCriteriaProcessor implements CriteriaProcessor {

	/**
	 * @var CriteriaProcessor[]
	 */
	private $criteriaProcesors = [];

	public function __construct(array $criteriaProcessors = []) {
		foreach ($criteriaProcessors as $criteriaProcessor) {
			$this->addCriteriaProcessor($criteriaProcessor);
		}
	}

	public function addCriteriaProcessor(CriteriaProcessor $citeriaProcessor) {
		$this->criteriaProcesors[] = $citeriaProcessor;
	}

	public function process(QueryBuilder $queryBuilder, $criteria) {
		foreach ($this->criteriaProcesors as $criteriaProcessor) {
			try {
				$criteriaProcessor->process($queryBuilder, $criteria);
				return;
			}
			catch (CannotProcessCriteria $exc) {
				
			}
		}

		throw new CannotProcessCriteria;
	}

}
