<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Doctrine;

use Doctrine\ORM\QueryBuilder;

interface CriteriaProcessor {

	public function process(QueryBuilder $queryBuilder, $criteria);
}
