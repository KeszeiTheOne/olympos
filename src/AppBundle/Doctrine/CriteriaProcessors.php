<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Doctrine;

use AppBundle\Doctrine\Criteria\Competition\CompetitionCriteriaProcessor;
use AppBundle\Doctrine\Criteria\DelegatingCriteriaProcessor;

class CriteriaProcessors {

	public static function createCriteriaProcessor() {
		return new DelegatingCriteriaProcessor([
			new CompetitionCriteriaProcessor()
		]);
	}

}
