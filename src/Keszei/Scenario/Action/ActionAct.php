<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Keszei\Scenario\Action;

use Keszei\Action\ActionRegistry;
use Keszei\Scenario\Act;
use Keszei\Scenario\RequestConverter;

class ActionAct implements Act {

	private $actionName;

	/**
	 * @var RequestConverter
	 */
	private $requestConverter;

	/**
	 * @var ActionRegistry
	 */
	private $actionRegistry;

	public function play($stimulus) {
		return $this->actionRegistry->runAction($this->actionName, $this->resolveRequest($stimulus));
	}

	private function resolveRequest($stimulus) {
		return $this->requestConverter->createRequest($stimulus);
	}

	public function setActionName($actionName) {
		$this->actionName = $actionName;
		return $this;
	}

	public function setRequestConverter(RequestConverter $requestConverter) {
		$this->requestConverter = $requestConverter;
		return $this;
	}

	public function setActionRegistry(ActionRegistry $actionRegistry) {
		$this->actionRegistry = $actionRegistry;
		return $this;
	}

}
