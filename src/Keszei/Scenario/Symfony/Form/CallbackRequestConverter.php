<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Keszei\Scenario\Symfony\Form;

use Keszei\Scenario\RequestConverter;

class CallbackRequestConverter implements RequestConverter {

	private $callback;

	public function __construct($callback) {
		$this->callback = $callback;
	}

	public function createRequest($stimulus) {
		return call_user_func($this->callback, $stimulus);
	}

}
