<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Keszei\Scenario\Symfony\Form\Exception;

use RuntimeException;
use Symfony\Component\Form\Form;

class ErroneousForm extends RuntimeException {

	/**
	 * @var Form
	 */
	private $form;

	public function __construct(Form $form) {
		parent::__construct();
		$this->form = $form;
	}

	public function getForm(): Form {
		return $this->form;
	}

}
