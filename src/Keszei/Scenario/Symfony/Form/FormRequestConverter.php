<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Keszei\Scenario\Symfony\Form;

use Keszei\Scenario\RequestConverter;
use Keszei\Scenario\Symfony\Form\Exception\InvalidForm;
use Keszei\Scenario\Symfony\Form\Exception\UnsubmittedForm;
use Symfony\Component\Form\FormFactory;

class FormRequestConverter implements RequestConverter {

	/**
	 * @var FormFactory
	 */
	private $formFactory;

	private $type;

	public function __construct(FormFactory $formFactory) {
		$this->formFactory = $formFactory;
	}

	public function createRequest($stimulus) {
		$form = $this->formFactory->create($this->type);
		$form->handleRequest($stimulus);

		if (!$form->isValid()) {
			throw new InvalidForm($form);
		}
		
		if (!$form->isSubmitted()) {
			throw new UnsubmittedForm($form);
		}
		
		return $form->getData();
	}

	public function setType($type) {
		$this->type = $type;
		return $this;
	}

}
