<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Keszei\Scenario\Narrator\ClientError;

use Keszei\Scenario\Narrator;

class ClientErrorNarrator implements Narrator {

	private $statusCode;

	private $message;

	public function __construct($statusCode, $message = "") {
		$this->statusCode = $statusCode;
		$this->message = $message;
	}

	public function tellStory($stimulus, $story) {
		return new ClientError($this->statusCode, $this->message);
	}

}
