<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Keszei\Scenario\Narrator\Template;

class Template {

	private $name;

	private $parameters;

	private $statusCode;

	public function __construct($name, array $parameters, $statusCode) {
		$this->name = $name;
		$this->parameters = $parameters;
		$this->statusCode = $statusCode;
	}

	public function getName() {
		return $this->name;
	}

	public function getParameters() {
		return $this->parameters;
	}

	public function getStatusCode() {
		return $this->statusCode;
	}

}
