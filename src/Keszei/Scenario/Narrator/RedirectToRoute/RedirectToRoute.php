<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Keszei\Scenario\Narrator\RedirectToRoute;

class RedirectToRoute {

	private $route;

	private $parameters = [];

	public function __construct($route, array $parameters) {
		$this->route = $route;
		$this->parameters = $parameters;
	}

	public function getRoute() {
		return $this->route;
	}

	public function getParameters() {
		return $this->parameters;
	}

}
