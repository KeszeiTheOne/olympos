<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Keszei\Scenario\Narrator\RedirectToRoute;

use Keszei\Scenario\Narrator;
use Keszei\Scenario\ParameterResolver;

class RedirectToRouteNarrator implements Narrator {

	private $route;

	private $parameters;

	public function __construct($route, $parameters = []) {
		$this->route = $route;
		$this->parameters = $parameters;
	}

	public function tellStory($stimulus, $story) {
		return new RedirectToRoute($this->route, $this->resolveParameters($stimulus, $story));
	}

	private function resolveParameters($stimulus, $story) {
		if ($this->parameters instanceof ParameterResolver) {
			return $this->parameters->tellStory($stimulus, $story);
		}

		return $this->parameters;
	}

}
