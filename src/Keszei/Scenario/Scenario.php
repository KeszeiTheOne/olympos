<?php

/*
 * Copyright 2019 Balázs Keszei <kebalazs95@gmail.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Keszei\Scenario;

use Exception;

class Scenario implements Act {

	/**
	 * @var Act
	 */
	private $act;

	/**
	 * @var Narrator
	 */
	private $narrator;

	/**
	 * @var Narrator[]
	 */
	private $exceptionalNarrators = [];

	public function __construct(Act $act, Narrator $narrator) {
		$this->act = $act;
		$this->narrator = $narrator;
	}

	public function play($stimulus) {
		try {
			return $this->narrator->tellStory($stimulus, $this->act->play($stimulus));
		}
		catch (Exception $exc) {
			return $this->getExceptionNarrator($exc)->tellStory($stimulus, $exc);
		}
	}

	/**
	 * @return Narrator
	 */
	private function getExceptionNarrator($exception) {
		foreach ($this->exceptionalNarrators as $exceptionClass => $narrator) {
			if (is_a($exception, $exceptionClass)) {
				return $narrator;
			}
		}

		throw $exception;
	}

	public function setExceptionalNarrators(array $exceptionalNarrators) {
		foreach ($exceptionalNarrators as $exceptionClass => $exceptionalNarrator) {
			$this->addExceptionalNarrator($exceptionClass, $exceptionalNarrator);
		}
		return $this;
	}

	public function addExceptionalNarrator($exceptionClass, Narrator $narrator) {
		$this->exceptionalNarrators[$exceptionClass] = $narrator;
		return $this;
	}

}
